package fr.bestteam.blacklist.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.bestteam.blacklist.Impl.PersonneServiceImpl;
import fr.bestteam.blacklist.bo.Personne;

@Controller
public class PersonneController {
	
	@Autowired
	PersonneServiceImpl manager;
	
	@RequestMapping("/")
	public String index(Model model){
		model.addAttribute("list", manager.getAll());
		return "index";
	}
	
	@GetMapping("/save/{id}")
	public String save(@PathVariable("id")Long id, Model model) {
		if(id!=null && id != 0) {
			model.addAttribute("personne", manager.get(id));
		}
		return "save";
		
	}
	
	@PostMapping("/save")
	public String save(Personne personne, Model model) {
		manager.save(personne);
		return "redirect:/";
}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id, Model model) {
		manager.delete(id);
		return "redirect:/";
		
	}
	
}