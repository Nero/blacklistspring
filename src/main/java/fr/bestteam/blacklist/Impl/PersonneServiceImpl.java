package fr.bestteam.blacklist.Impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import fr.bestteam.blacklist.bll.PersonneService;
import fr.bestteam.blacklist.bo.Personne;
import fr.bestteam.blacklist.dal.PersonneDAO;
import fr.bestteam.blacklist.generics.GenericDAO;


@Service
public class PersonneServiceImpl implements PersonneService {
	
	@Autowired
	PersonneDAO dao;

	@Override
	public Personne save(Personne personne) {
		
		return  dao.save(personne);
	}

	@Override
	public void delete(Long id) {
		dao.deleteById(id);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Personne get(Long id) {
		
		return ((GenericDAO<Personne, Long>) dao).get(id);
	}

	@Override
	public List<Personne> getAll() {
		
		return (List<Personne>) dao.findAll();
	}

	
	

	

	
}
