package fr.bestteam.blacklist.generics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public abstract class GenericDAOImpl<T, ID extends Serializable> implements GenericDAO<T, Serializable> {

	@Override
	public T save(T entity) {
	
		return getDAO().save(entity);
	}

	@Override
	public void delete(Serializable id) {
		getDAO().deleteById((ID) id);
		
	}

	@Override
	public T get(Serializable id) {
		Optional<T>obj = getDAO().findById((ID) id);
		if(obj.isPresent()) {
			return obj.get();
		}
		return null;
	}

	@Override
	public List<T> getAll() {
		List<T> returnList = new ArrayList<>();
		getDAO().findAll().forEach(obj -> returnList.add(obj));
		return returnList;
	}
	
	public abstract CrudRepository<T, ID> getDAO();
	
}
