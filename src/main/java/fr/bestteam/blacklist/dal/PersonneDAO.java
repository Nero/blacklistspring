package fr.bestteam.blacklist.dal;

import org.springframework.data.repository.CrudRepository;

import fr.bestteam.blacklist.bo.Personne;


	
	public interface PersonneDAO extends CrudRepository<Personne, Long>{

}
