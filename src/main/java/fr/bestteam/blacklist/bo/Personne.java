package fr.bestteam.blacklist.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Personne {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String nom;
	
	@Column
	private String prenom;
	
	@Column
	private String Commentaire;
	
	@Column
	private String Provenance;

	
	

	public Personne(String nom, String prenom, String commentaire, String provenance) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		Commentaire = commentaire;
		Provenance = provenance;
	}


	public Personne() {
		
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	

	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getCommentaire() {
		return Commentaire;
	}


	public void setCommentaire(String commentaire) {
		Commentaire = commentaire;
	}


	public String getProvenance() {
		return Provenance;
	}


	public void setProvenance(String provenance) {
		Provenance = provenance;
	}


	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", Commentaire=" + Commentaire
				+ ", Provenance=" + Provenance + "]";
	}


	
	
	
}
